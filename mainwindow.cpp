#include "mainwindow.h"
#include "berlekemp_messy_alg.h"
#include "reed_solomon_alg.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , saver(std::make_shared<JSONAlgorithmStateStorage>())
    , generations(std::make_shared<std::vector<Generation::Generation_result>>())
    , algorithm(std::make_shared<Berlekemp_Messy_alg>(saver))
    , manager(std::make_shared<AlgorithmManager>(p, e))
    , overall_calcualations(0)
    , current_calc_count(0)
{
    ui->setupUi(this);

    generation = std::make_shared<Generation>(parse_roots_file("roots.csv"));

    //generation signals
    connect(generation.get(), &Generation::generationCompleted, this, &MainWindow::generation_finished);
    connect(generation.get(), &Generation::generationError, this, &MainWindow::generation_error);
    connect(generation.get(), &Generation::generationStopped, this, &MainWindow::generation_stopped);

    //Storage signals
    connect(saver.get(), &AbstractAlgorithmStateStorage::sendMsg, this, &MainWindow::gotStorageMsg);

    //Abstract algorithm
    initAlgorithm();

    //Manager signals
    connect(manager.get(), &AlgorithmManager::currentCalcFinished, this, &MainWindow::handle_result);
    connect(manager.get(), &AlgorithmManager::calcStopped, this, &MainWindow::stop_from_alg);
    connect(this, &MainWindow::stop_calculations, manager.get(), &AlgorithmManager::stopCalculations);
    connect(manager.get(), &AlgorithmManager::allCalcFinished, this, &MainWindow::all_calc_finished);

    connect(ui->pb_stop_gen, &QPushButton::clicked, generation.get(), &Generation::stopGeneration);

    checkPBLoadEnabled();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_generate_clicked()
{
    //Handles click on generate button

    //init Generation
    int p_min = ui->sb_p_min->value();
    int p_max = ui->sb_p_max->value();
    int n = ui->sb_n->value();
    int f = ui->sb_f->value();
    int a = ui->sb_a->value();
    int b = ui->sb_b->value();
    int c = ui->sb_c->value();
    int d = ui->sb_d->value();

    generation->init(p_min, p_max, n, f, a, b, c, d);

    //start generation process
    if (generation->startGeneration())
    {
        ui->status_edit->setText("Последовательность генерируется...");
        ui->pb_generate->setEnabled(false);
        ui->pb_calculate->setEnabled(false);
        ui->pb_stop_gen->setEnabled(true);
    }
}

void MainWindow::on_pb_calculate_clicked()
{
    if (generations == nullptr || generations->empty())
    {
        ui->pb_calculate->setEnabled(false);
        QMessageBox::warning(this, "Ошибка", "Необходимо сначала сгенерировать последовательность");
        return;
    }

    if(manager == nullptr || algorithm == nullptr)
        return;

    initCalculations();


    manager->startCalculations(algorithm, generations, ui->output_filename->text(), ui->cb_save_gen->isChecked());
}

void MainWindow::handle_result(const int result)
{
    //on result of algorithm from algorith thread
    //update overall progress according to amount of calculations

    current_calc_count++;
    ui->progress_overall->setValue(static_cast<double>(current_calc_count) / overall_calcualations * 100);
    qDebug() << "result = " << result;
}

void MainWindow::algorithm_progress_update(const float progress)
{
    //on update of current progress of algorithm
    ui->progress_current->setValue(progress);
}

void MainWindow::all_calc_finished()
{
    //algorithm thread finished all calculations
    ui->status_edit->setText("Все расчеты завершены");
    ui->progress_current->setValue(100);
    ui->progress_overall->setValue(100);
    start_stop_enable(true);
    ui->pb_save->setEnabled(false);
    ui->pb_resume->setEnabled(false);
}

void MainWindow::on_pb_stop_calc_clicked()
{
    //algorithm thread should stop calculations upon getting this signal
    emit stop_calculations();
}

void MainWindow::stop_from_alg()
{
    //algorithm thread was stopped by user
    ui->status_edit->setText("Расчеты остановлены");
    start_stop_enable(true);
    ui->pb_save->setEnabled(true);
    ui->pb_resume->setEnabled(true);
}

void MainWindow::generation_finished(std::shared_ptr<std::vector<Generation::Generation_result>> result)
{
    //generation was successfully finished
    generations = result;
    ui->status_edit->setText("Последовательность сгенерирована");
    overall_calcualations = generations->size();
    ui->pb_generate->setEnabled(true);
    ui->pb_calculate->setEnabled(true);
    ui->pb_stop_gen->setEnabled(false);
}

void MainWindow::generation_error(std::shared_ptr<QString> error)
{
    //generation exited with an error
    ui->status_edit->setText(*error.get());
    ui->pb_generate->setEnabled(true);
}

void MainWindow::generation_stopped()
{
    //generation was stopped by user
    ui->status_edit->setText("Генерация остановлена!");
    ui->pb_generate->setEnabled(true);
    ui->pb_stop_gen->setEnabled(false);
}

void MainWindow::start_stop_enable(const bool enabled)
{
    //switches start and stop buttons for calculations
    ui->pb_stop_calc->setEnabled(!enabled);
    ui->pb_calculate->setEnabled(enabled);
}

void MainWindow::initCalculations()
{
    //init before calculations
    ui->pb_save->setEnabled(false);
    start_stop_enable(false);

    current_calc_count = 0;
    ui->progress_overall->setValue(0);
    ui->progress_current->setValue(0);
    ui->status_edit->setText("Расчеты выполняются...");
}

void MainWindow::initAlgorithm()
{
    //init algorithm signals
    if(algorithm == nullptr)
        return;
    connect(algorithm.get(), &Abstract_algorithm::update_progress, this, &MainWindow::algorithm_progress_update);
    connect(algorithm.get(), &Abstract_algorithm::restoreSuccess, this, &MainWindow::restoredSuccuss);
    connect(algorithm.get(), &Abstract_algorithm::restoreError, this, &MainWindow::restoredError);
    connect(algorithm.get(), &Abstract_algorithm::storeSuccess, this, &MainWindow::storedSuccuss);
    connect(algorithm.get(), &Abstract_algorithm::storeError, this, &MainWindow::storedError);
}

void MainWindow::checkPBLoadEnabled()
{
    //sets pushbutton Load enabled only if current algorithm can load that
    ui->pb_load->setEnabled(algorithm != nullptr && algorithm->canRestore(JSONpath));
}

std::vector<Generation::root> MainWindow::parse_roots_file(const QString& filename)
{
    //open and parse roots file
    //it's needed for Generation
    //files is in a CSV format
    //each line contains 2 integer numbers separated by ";"
    QFile input_file(filename);
    std::vector<Generation::root> roots;
    if (!input_file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Error: unable to open file" << input_file.fileName();
        ui->status_edit->setText("Не удалось открыть файл" + input_file.fileName());
        return roots;
    }

    QTextStream in(&input_file);
    QString line = in.readLine();
    while (!line.isNull())
    {
        QStringList list = line.split(";");
        bool is_s_ok = false;
        bool is_g_ok = false;
        auto simple = list[0].toUInt(&is_s_ok);
        auto g = list[1].toUInt(&is_g_ok);

        if (is_s_ok && is_g_ok)
            roots.push_back({ simple, g });
        else
        {
            qDebug() << "Error: while parsing roots: " << line;
            ui->status_edit->setText("Файл корней поврежден");
        }

        line = in.readLine();
    }
    return roots;
}

void MainWindow::on_rb_ring_toggled(bool checked)
{
    //algorithm was changed => reinit
    if (checked)
    {
        algorithm = std::make_shared<Reed_Solomon_alg>(saver);
        initAlgorithm();
        checkPBLoadEnabled();
        ui->pb_save->setEnabled(false);
        ui->pb_resume->setEnabled(false);
    }
}

void MainWindow::on_rb_field_toggled(bool checked)
{
    //algorithm was changed => reinit
    if (checked)
    {
        algorithm = std::make_shared<Berlekemp_Messy_alg>(saver);
        initAlgorithm();
        checkPBLoadEnabled();
        ui->pb_save->setEnabled(false);
        ui->pb_resume->setEnabled(false);
    }
}

void MainWindow::on_pb_save_clicked()
{
    //saves current algorithm progress
    if (algorithm != nullptr)
    {
        algorithm->store(JSONpath);
        checkPBLoadEnabled();
    }
}

void MainWindow::on_pb_load_clicked()
{
    //loads saved algorithm progress
    if (algorithm != nullptr)
    {
        algorithm->restore(JSONpath);
    }
}

void MainWindow::on_pb_resume_clicked()
{
    //inits a new thread for resuming calculations
    initCalculations();

    manager->resumeCalculations(ui->output_filename->text(), ui->cb_save_gen->isChecked());
}

void MainWindow::gotStorageMsg(std::shared_ptr<QString> msg)
{
    //show message from storage
    if(msg != nullptr)
        qDebug() << *msg.get();
}

void MainWindow::restoredSuccuss()
{
    //algorithm restored data succuessfully

    ui->status_edit->setText("Текущее состояние успешно загружено!");
    ui->progress_current->setValue(0);
    ui->progress_overall->setValue(0);
    ui->pb_resume->setEnabled(true);
}

void MainWindow::restoredError(std::shared_ptr<QString> msg)
{
    //algorithm wasn't able to restore data
    QString text("Ошибка загрузки: ");
    if(msg != nullptr)
        text.append(*msg.get());
    ui->status_edit->setText(text);
    ui->pb_resume->setEnabled(false);
}

void MainWindow::storedSuccuss()
{
    //algorithm stored data succuessfully
    ui->status_edit->setText("Текущее состояние успешно сохранено!");
}

void MainWindow::storedError(std::shared_ptr<QString> msg)
{
    //algorithm wasn't able to store data
    QString text("Ошибка сохранения: ");
    if(msg != nullptr)
        text.append(*msg.get());
    ui->status_edit->setText(text);
}
