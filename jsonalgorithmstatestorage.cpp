#include "jsonalgorithmstatestorage.h"
#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>

JSONAlgorithmStateStorage::JSONAlgorithmStateStorage()
{
}

bool JSONAlgorithmStateStorage::putName(const QString& data)
{
    json["name"] = data;
    return true;
}

bool JSONAlgorithmStateStorage::putInt(const QString& name, const int data)
{
    json[name] = data;
    return true;
}

bool JSONAlgorithmStateStorage::putUInt(const QString& name, const uint data)
{
    json[name] = static_cast<double>(data);
    return true;
}

bool JSONAlgorithmStateStorage::putVector(const QString& name, const std::vector<int>& data)
{
    QJsonArray arr;
    for (auto item : data)
        arr.push_back(item);
    json[name] = arr;
    return true;
}

bool JSONAlgorithmStateStorage::putVectorOfVectors(const QString& name, const std::vector<std::vector<int>>& data)
{
    QJsonArray doubleArr;
    for (uint i = 0; i < data.size(); i++)
    {
        QJsonArray arr;

        for (auto item : data[i])
            arr.push_back(item);

        doubleArr.push_back(arr);
    }

    json.insert(name, doubleArr);
    return true;
}

bool JSONAlgorithmStateStorage::saveIt(const QString& path)
{
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "JSON Saver::saveIt unable to open" << path;
        emit sendMsg(std::make_shared<QString>("Не удалось открыть файл для сохранения " + path));
        return false;
    }
    else
    {
        file.write(QJsonDocument(json).toJson());
        file.close();
        return true;
    }
}

bool JSONAlgorithmStateStorage::loadIt(const QString& path)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "JSON Saver::loadIt unable to open" << path;
        emit sendMsg(std::make_shared<QString>("Не удалось открыть файл для восстановления " + path));
        return false;
    }
    else
    {
        QString val;
        val = file.readAll();
        file.close();
        QJsonDocument jsondoc = QJsonDocument::fromJson(val.toUtf8());
        json = jsondoc.object();
        return true;
    }
}

bool JSONAlgorithmStateStorage::getName(QString& result)
{
    if (json.contains("name") && json["name"].isString())
    {
        result = json["name"].toString();
        return true;
    }
    return false;
}

bool JSONAlgorithmStateStorage::getInt(const QString& name, int& result)
{
    if (json.contains(name) && json[name].isDouble())
    {
        result = json[name].toInt();
        return true;
    }
    return false;
}

bool JSONAlgorithmStateStorage::getUInt(const QString& name, uint& result)
{
    if (json.contains(name) && json[name].isDouble())
    {
        result = static_cast<uint>(json[name].toDouble());
        return true;
    }
    return false;
}
bool JSONAlgorithmStateStorage::getVector(const QString& name, std::vector<int>& result)
{
    if (json.contains(name) && json[name].isArray())
    {
        result.clear();
        auto res = json[name].toArray();
        for (auto item : res)
            result.push_back(item.toInt());

        return true;
    }
    return false;
}


bool JSONAlgorithmStateStorage::getVectorOfVectors(const QString &name, std::vector<std::vector<int>> &result)
{
    if (json.contains(name) && json[name].isArray())
    {
        result.clear();
        auto arrays = json[name].toArray();
        for(int i = 0; i < arrays.size(); i++)
        {
            if(arrays[i].isArray())
            {
                std::vector<int> res;
                auto array = arrays[i].toArray();
                for(auto item : array)
                    res.push_back(item.toInt());
                result.push_back(res);
            }
        }
        return true;
    }
    return false;
}


void JSONAlgorithmStateStorage::clear()
{
    json = QJsonObject();
}
