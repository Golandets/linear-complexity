#include "berlekemp_messy_alg.h"

Berlekemp_Messy_alg::Berlekemp_Messy_alg(const std::shared_ptr<AbstractAlgorithmStateStorage> i_state_saver)
    : Abstract_algorithm("Алгоритм Берлекэмпа-Месси", i_state_saver)
{
}

int Berlekemp_Messy_alg::calculate()
{
    clear();

    //init
    g.resize(S.size());
    g[0] = 1;
    g.resize(get_num_of_vals(g));

    h.resize(S.size());
    h[1] = 1;
    h.resize(get_num_of_vals(h));

    m = 0;

    gG = mult(g, S);
    b = S[0]; //g0*G и вначале g[0] = 1

    //        qDebug() << "Step 0";
    //        print_vector(g, "g: ");
    //        print_vector(h, "h: ");
    //                print_vector(G, "G: ");
    //        print_vector(gG, "gG:");
    //        qDebug() << "b=" << b << "m=" << m;

    //for P(n^2)
    qDebug() << "Calculate for size=" << S.size();

    step = 1;
    return doCalculations();
}

int Berlekemp_Messy_alg::resumeCalculations()
{
    return doCalculations();
}

void Berlekemp_Messy_alg::clear()
{
    g.clear();
    h.clear();
    gG.clear();
}

void Berlekemp_Messy_alg::restore(const QString& path)
{
    //stores all inside data in storage
    if (stateSaver != nullptr && stateSaver->loadIt(path))
    {
        stateSaver->getUInt(storageStepName, step);
        stateSaver->getUInt(storagePName, p);
        stateSaver->getUInt(storageEName, e);

        stateSaver->getInt(storageBName, b);
        stateSaver->getInt(storageMName, m);

        stateSaver->getVector(storageSName, S);
        stateSaver->getVector(storageHName, h);
        stateSaver->getVector(storageGName, g);

        restoreGeneration();

        emit restoreSuccess();
    }
    else
        emit restoreError(std::make_shared<QString>("Не удалось открыть файл " + path));
}

void Berlekemp_Messy_alg::store(const QString& path) const
{
    //restores all data from storage
    if (stateSaver != nullptr)
    {
        stateSaver->clear();

        stateSaver->putName(getName());

        stateSaver->putUInt(storageStepName, step);
        stateSaver->putUInt(storagePName, p);
        stateSaver->putUInt(storageEName, e);

        stateSaver->putInt(storageBName, b);
        stateSaver->putInt(storageMName, m);

        stateSaver->putVector(storageSName, S);
        stateSaver->putVector(storageHName, h);
        stateSaver->putVector(storageGName, g);

        storeGeneration();

        if(stateSaver->saveIt(path))
            emit storeSuccess();
        else
            emit storeError(std::make_shared<QString>("Не удалось сохранить файл " + path));
    }
}

QString Berlekemp_Messy_alg::restoreName(const QString& path) const
{
    QString name;
    if (stateSaver != nullptr && stateSaver->loadIt(path))
        stateSaver->getName(name);
    return name;
}

int Berlekemp_Messy_alg::doCalculations()
{
    //main cycle of the algorithm
    should_stop.store(false);

    std::vector<int> x;
    x.resize(S.size());
    x[1] = 1;

    for (; step <= S.size(); step++)
    {
        if (should_stop.load())
        {
            qDebug() << "user stopped";
            return -1;
        }

#if SHOW_PROGRESS
        float progress = step * 100.0 / S.size();
        emit update_progress(progress);
#endif //SHOW_PROGRESS

        auto temp_g = g;
        g = sub(g, mult(h, b));

        if (b != 0 && m >= 0)
        {
            auto temp = mult(b, b);
            h = mult(mult(temp_g, x), temp);
            m = -m;
        }
        else
        {
            h = mult(x, h);
            m++;
        }

        gG = mult(g, S);
        b = gG[step];

        //            qDebug() << "Step" << i;
        //            print_vector(g, "g: ");
        //            print_vector(h, "h: ");
        //            print_vector(gG, "gG:");
        //            qDebug() << "b=" << b << "m=" << m;
    }
    //возвращаем линейную сложность L
    for (int i = g.size() - 1; i >= 0; i--)
        if (g[i] != 0)
            return i;
    return 0;
}

std::vector<int> Berlekemp_Messy_alg::add(const std::vector<int> val1, const std::vector<int> val2) const
{
    std::vector<int> res;
    res.resize(std::max(val1.size(), val2.size()));
    for (uint i = 0; i < res.size(); i++)
    {
        if (i < val1.size())
            res[i] = add(res[i], val1[i]);
        if (i < val2.size())
            res[i] = add(res[i], val2[i]);
    }

    res.resize(get_num_of_vals(res));
    return res;
}

std::vector<int> Berlekemp_Messy_alg::sub(const std::vector<int> val1, const std::vector<int> val2) const
{
    return add(val1, val2);
}

std::vector<int> Berlekemp_Messy_alg::mult(const std::vector<int> val1, const std::vector<int> val2) const
{
    //умножение 2 полиномов
    std::vector<int> res;
    res.resize(val1.size() + val2.size() - 1);
    for (uint i = 0; i < val1.size(); i++)
        for (uint j = 0; j < val2.size(); j++)
        {
            auto temp1 = mult(val1[i], val2[j]);
            auto temp = add(res[i + j], temp1);
            res[i + j] = temp;
        }

    res.resize(get_num_of_vals(res));
    return res;
}

std::vector<int> Berlekemp_Messy_alg::mult(const std::vector<int> val1, const int val2) const
{
    //умножение полинома на число
    std::vector<int> res;
    res.resize((val1.size()) + 1);
    for (uint i = 0; i < val1.size(); i++)
        res[i] = mult(val1[i], val2);

    res.resize(get_num_of_vals(res));
    return res;
}

uint Berlekemp_Messy_alg::get_num_of_vals(const std::vector<int> &val) const
{
    //возвращает количество значимых элементов вектора
    //нужно, чтобы они не росли в длинну при умножении
    for (uint i = val.size() - 1; i > 0; i--)
        if (val[i] != 0)
            return i + 1;
    return 1;
}

QString Berlekemp_Messy_alg::val_to_str(const int val, const int pos) const
{
    //преобразуем значение в удобочитаемую форму
    QString res;
    if (val)
    {
        if (val > 1 || pos == 0)
            res.append(QString::number(val));
        if (pos > 1)
            res.append("x^").append(QString::number(pos));
        else if (pos > 0)
            res.append("x");
        res.append("+");
    }
    return res;
}

void Berlekemp_Messy_alg::print_vector(const std::vector<int> &val, const QString &&name) const
{
    //выводим значение вектора в удобной форме
    QString outp = name;
    for (uint i = 0; i < val.size(); i++)
        outp.append(val_to_str(val[i], i));
    outp.remove(outp.length() - 1, 1);
    qDebug() << outp;
}

int Berlekemp_Messy_alg::add(const int a, const int b) const
{
    if ((static_cast<uint>(a) < plus_table.size())
        && (static_cast<uint>(b) < plus_table.size()))
    {
        return plus_table[a][b];
    }
    return -1;
}

int Berlekemp_Messy_alg::mult(const int a, const int b) const
{
    if ((static_cast<uint>(a) < mult_table.size())
        && (static_cast<uint>(b) < mult_table.size()))
    {
        return mult_table[a][b];
    }
    return -1;
}
