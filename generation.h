#ifndef GENERATION_H
#define GENERATION_H
#include <QFile>
#include <QMetaType>
#include <atomic>
#include <future>
#include <vector>

//! Generation is a class for generating cyclotomic forth-order sequences
//! with given parameters
class Generation : public QObject
{
    Q_OBJECT
public:
    //! Structure with a resulting generated sequence and given parameters
    struct Generation_result
    {
        std::vector<int> generation;
        uint p;
        uint pn_2;
        uint n;
        uint f;
        int a;
        int b;
        int c;
        int d;

        bool isParamsEqual(const Generation::Generation_result& rhs) const
        {
            return n == rhs.n && f == rhs.f && a == rhs.a && b == rhs.b && c == rhs.c && d == rhs.d;
        }
    };
    //! Structure with simple numbers and antiderivative root mod p^n for those numbers
    struct root
    {
        uint simple;
        uint g;
    };

    Generation(const std::vector<root> i_roots);
    //initializes Generation with given parameters
    void init(uint32_t i_p_min, uint32_t i_p_max, uint32_t i_n, uint32_t i_f,
        int i_a, int i_b, int i_c, int i_d);

    //starts generation process in async mode
    bool startGeneration();

    ~Generation();

public slots:
    void stopGeneration();

private:
    //converts given value to mod value
    int64_t fMod(const int64_t& val, const int64_t& mod) const;

    //raises and mods base to the power of degree
    int64_t modpow(const int64_t& base, const int64_t& degree, const int64_t& mod) const;

    //generates sequences with given parameters and emits signal
    void generate();

    QFile input_file;
    std::vector<root> roots;

    uint32_t p_min;
    uint32_t p_max;
    uint32_t n;
    uint32_t f;
    int a;
    int b;
    int c;
    int d;
    std::future<void> future;
    std::atomic<bool> should_stop;

signals:
    void generationCompleted(std::shared_ptr<std::vector<Generation::Generation_result>>);
    void generationError(std::shared_ptr<QString>);
    void generationStopped();
};
#endif // GENERATION_H
