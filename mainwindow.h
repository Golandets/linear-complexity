#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "abstract_algorithm.h"
#include <QMainWindow>

#include <memory>
#include <vector>

#include "generation.h"
#include "jsonalgorithmstatestorage.h"
#include "algorithmmanager.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void on_pb_generate_clicked();

    void on_pb_calculate_clicked();

    void handle_result(const int result);

    void algorithm_progress_update(const float progress);

    void all_calc_finished();

    void on_pb_stop_calc_clicked();

    void stop_from_alg();

    void generation_finished(std::shared_ptr<std::vector<Generation::Generation_result>> result);

    void generation_error(std::shared_ptr<QString> error);

    void generation_stopped();

    void on_rb_ring_toggled(bool checked);

    void on_rb_field_toggled(bool checked);

    void on_pb_save_clicked();

    void on_pb_load_clicked();

    void on_pb_resume_clicked();

    void gotStorageMsg(std::shared_ptr<QString> msg);

    void restoredSuccuss();

    void restoredError(std::shared_ptr<QString> msg);

    void storedSuccuss();

    void storedError(std::shared_ptr<QString> msg);

private:
    void start_stop_enable(const bool enabled);
    void initCalculations();
    void initAlgorithm();
    void checkPBLoadEnabled();

    std::vector<Generation::root> parse_roots_file(const QString& filename);
    Ui::MainWindow* ui;

    const int p = 2;
    const int e = 2;

    std::shared_ptr<AbstractAlgorithmStateStorage> saver;
    std::shared_ptr<std::vector<Generation::Generation_result>> generations;
    std::shared_ptr<Abstract_algorithm> algorithm;
    std::shared_ptr<Generation> generation;
    std::shared_ptr<AlgorithmManager> manager;

    uint32_t overall_calcualations;
    uint32_t current_calc_count;


    const QString JSONpath = "algsaves.json";

signals:
    void stop_calculations();
};
#endif // MAINWINDOW_H
