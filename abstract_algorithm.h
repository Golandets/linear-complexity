#ifndef ABSTRACT_ALGORITHM_H
#define ABSTRACT_ALGORITHM_H
#include <QObject>
#include <atomic>
#include <memory>
#include <vector>

#include "abstractalgorithmstatestorage.h"
#include "generation.h"

#define SHOW_PROGRESS 1

class Abstract_algorithm : public QObject
{
    Q_OBJECT
public:
    Abstract_algorithm(const QString& i_name, const std::shared_ptr<AbstractAlgorithmStateStorage> i_state_saver);
    virtual void init(Generation::Generation_result & gen, const uint i_p, const uint i_e);

    virtual int calculate() = 0;

    virtual int resumeCalculations() = 0;

    virtual void stop();

    virtual QString getName() const;

    virtual Generation::Generation_result getGeneration() const;

    virtual bool canRestore(const QString& path) const;

    virtual void restore(const QString& path) = 0;

    virtual void store(const QString& path) const = 0;

protected:
    virtual QString restoreName(const QString& path) const = 0;

    virtual void restoreGeneration();
    virtual void storeGeneration() const;

    int fMod(const int a, const int p) const;

    const QString name;
    Generation::Generation_result generation;
    std::vector<int> S;
    uint p;
    uint e;
    uint step;
    const QString storageStepName = "step";
    const QString storageSName = "S";
    const QString storagePName = "p";
    const QString storageEName = "e";

    //for store and restore GenerationResult
    const QString storageGenName = "gen_generation";
    const QString storageGenPName = "gen_p";
    const QString storageGenPn2Name = "gen_pn_2";
    const QString storageGenNName = "gen_n";
    const QString storageGenFName = "gen_f";
    const QString storageGenAName = "gen_a";
    const QString storageGenBName = "gen_b";
    const QString storageGenCName = "gen_c";
    const QString storageGenDName = "gen_d";

    std::atomic<bool> should_stop;

    std::shared_ptr<AbstractAlgorithmStateStorage> stateSaver;

signals:
    void update_progress(const float);
    void restoreSuccess();
    void restoreError(std::shared_ptr<QString>);
    void storeSuccess() const;
    void storeError(std::shared_ptr<QString>) const;
};

#endif // ABSTRACT_ALGORITHM_H
