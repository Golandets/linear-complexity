#include "algorithmmanager.h"
#include "generation.h"

#include <QTextStream>
#include <QDebug>

AlgorithmManager::AlgorithmManager(const uint i_p, const uint i_e)
    : p(i_p)
    , e(i_e)
    , initFileStr("p;2p^n;L;n;f;A;B;C;D;Алгоритм;s;")
{
    qRegisterMetaType<std::shared_ptr<std::vector<int>>>("std::shared_ptr<std::vector<int>>");
    stopped.store(false);
}

AlgorithmManager::~AlgorithmManager()
{
    //if calculations were active stop them
    stopCalculations();
}

void AlgorithmManager::startCalculations(std::shared_ptr<Abstract_algorithm> i_alg,
                                          std::shared_ptr<std::vector<Generation::Generation_result> > generations,
                                          const QString &i_fileName, const bool i_saveGen)
{
    //starts calculations of given generations with selected Algorithm

    algorithm = i_alg;
    filename = i_fileName;
    saveGenerations = i_saveGen;

    output_file.setFileName(filename);

    results.clear();
    stopped.store(false);

    if (algorithm == nullptr)
    {
        emit calcError("Ошибка: Не выбран алгоритм поиска линейной сложности");
        return;
    }

    if(generations == nullptr || generations->size() == 0)
    {
        emit calcError("Ошибка: Отстствуют входные последовательности");
        return;
    }


    initFile();

    fut = std::async(std::launch::async, [this, generations] {

        std::vector<Generation::Generation_result> gens = *generations.get();

        for(auto& gen : gens)
        {
            //user stopped calculations
            if (stopped.load())
            {
                emit calcStopped();
                return;
            }

            algorithm->init(gen, p, e);

            std::chrono::high_resolution_clock clock;
            auto current = clock.now();

            auto res = algorithm->calculate();

            auto duration = clock.now() - current;
            qDebug() << "time for p =" << gen.p << ":" << std::chrono::duration_cast<std::chrono::microseconds>(duration).count() / 1000000.0 << "s";


            if(res == -1 && stopped.load())
            {
                emit calcStopped();
                return;
            }

            results.push_back(res);

            saveToFile(algorithm->getGeneration(), res);

            emit currentCalcFinished(res);

        }

        emit allCalcFinished(std::make_shared<std::vector<int>>(results));
    });

}

void AlgorithmManager::resumeCalculations(const QString & i_fileName, const bool i_saveGen)
{
    //resumes last calculation
    filename = i_fileName;
    saveGenerations = i_saveGen;

    output_file.setFileName(filename);

    stopped.store(false);

    if (algorithm == nullptr)
    {
        emit calcError("Ошибка: Не выбран алгоритм поиска линейной сложности");
        return;
    }

    fut = std::async(std::launch::async, [this] {
        auto res = algorithm->resumeCalculations();
        if (res == -1 && stopped.load())
        {
            emit calcStopped();
            return;
        }
        results.push_back(res);

        saveToFile(algorithm->getGeneration(), res);

        emit currentCalcFinished(res);

        emit allCalcFinished(std::make_shared<std::vector<int>>(results));
    });
}

void AlgorithmManager::stopCalculations()
{
    qDebug() << "AlgorithmThread: stop_calculations";
    stopped.store(true);

    if (algorithm != nullptr)
        algorithm->stop();
}

void AlgorithmManager::saveToFile(Generation::Generation_result gen, int result)
{
    //open file and save all results
    if (!output_file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        qDebug() << "Error: unable to open file" << output_file.fileName();
        emit calcError("Ошибка: Не удалось открыть файл" + output_file.fileName());
        return;
    }

    QTextStream out(&output_file);

    out << gen.p << ";" << gen.pn_2 << ";" << result << ";" << gen.n << ";" << gen.f << ";" << gen.a << ";"
        << gen.b << ";" << gen.c << ";" << gen.d << ";";
    if (algorithm != nullptr)
        out << algorithm->getName();

    out << ";";

    //if user wants to save generation as well
    if (saveGenerations)
    {
        for (auto a : gen.generation)
        {
            out << a << ";";
        }
    }
    out << endl;

    output_file.close();
}

void AlgorithmManager::initFile()
{
    if (!output_file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        qDebug() << "Error: unable to open file" << output_file.fileName();
        emit calcError("Ошибка: Не удалось открыть файл" + output_file.fileName());
        return;
    }
    if (output_file.size() == 0)
    {
        //means file is empty
        //put initial values
        QTextStream out(&output_file);

        out << initFileStr << endl;
    }
    output_file.close();
}
