#ifndef JSONALGORITHMSTATESAVER_H
#define JSONALGORITHMSTATESAVER_H
#include "abstractalgorithmstatestorage.h"
#include <QJsonObject>

class JSONAlgorithmStateStorage : public AbstractAlgorithmStateStorage
{
public:
    JSONAlgorithmStateStorage();

    // AbstractAlgorithmStateSaver interface
public:
    virtual bool putName(const QString& data) override;
    virtual bool putInt(const QString& name, const int data) override;
    virtual bool putUInt(const QString& name, const uint data) override;
    virtual bool putVector(const QString& name, const std::vector<int>& data) override;
    virtual bool putVectorOfVectors(const QString& name, const std::vector<std::vector<int>>& data) override;

    virtual bool saveIt(const QString& path) override;
    virtual bool loadIt(const QString& path) override;

    virtual bool getName(QString& result) override;
    virtual bool getInt(const QString& name, int& result) override;
    virtual bool getUInt(const QString& name, uint& result) override;
    virtual bool getVector(const QString& name, std::vector<int>& result) override;
    virtual bool getVectorOfVectors(const QString &name, std::vector<std::vector<int>> &result) override;

    virtual void clear() override;
protected:
    QJsonObject json;
};

#endif // JSONALGORITHMSTATESAVER_H
