Linear Complexity program for the analysis of the linear complexity of the quaternary sequences.
This software will serve to test and illustrate the theoretical results on the linear complexity of
quaternary sequences, to generate and determine the parameters of the quaternary
generalized cyclotomic sequences with high linear complexity over a finite field or
fourth-order ring. Performing this task manually is very labor intensive and the
program under development will allow to simplify and speed up calculations,
which allows to reduce human labor costs.

Program can generate cyclotomic quaternary sequences with given parameters
and calculate linear complexity of that sequences over a finite field or
fourth-order ring.

Berlekamp–Massey algorithm is implemented to calculated linear complexity over the forth-order finite field

Reed-Solomon algorithm is implemented to calculate linear complexity over the forth-order ring

File roots.csv is needed for the Generation. It contains numbers p and antiderivative root mod p^n for each p.
It has to be put in directory with .exe file, program tries to read from that file in runtime and Error is shown
if file wasn't found.
