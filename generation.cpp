#include "generation.h"
#include "berlekemp_messy_alg.h"
#include <QDebug>
#include <windows.h>

Generation::Generation(const std::vector<root> i_roots)
    : roots(i_roots)
{
    qRegisterMetaType<std::shared_ptr<std::vector<Generation::Generation_result>>>("std::shared_ptr<std::vector<Generation::Generation_result>>");

    should_stop.store(false);
}

Generation::~Generation()
{
}

void Generation::stopGeneration()
{
    should_stop.store(true);
}

void Generation::init(uint32_t i_p_min, uint32_t i_p_max, uint32_t i_n, uint32_t i_f, int i_a, int i_b, int i_c, int i_d)
{
    p_min = i_p_min;
    p_max = i_p_max;
    n = i_n;
    f = i_f;
    a = i_a;
    b = i_b;
    c = i_c;
    d = i_d;
}

bool Generation::startGeneration()
{
    if (roots.size() > 0)
    {
        should_stop.store(false);
        future = std::async(std::launch::async, [this] {
            generate();
        });
        return true;
    }
    emit generationError(std::make_shared<QString>("Не удалось начать генерацию: отсутствует файл корней"));
    return false;
}

int64_t Generation::fMod(const int64_t& val, const int64_t& mod) const
{
    if (val >= 0)
        return (val % mod);
    else
        return (val + (val % mod));
}

int64_t Generation::modpow(const int64_t& base, const int64_t& degree, const int64_t& mod) const
{
    int64_t result = 1;
    if (degree != 0)
        result = base;
    auto d = degree;
    while (--d > 0)
    {
        result = fMod(result * base, mod);
    }
    return result;
}

void Generation::generate()
{
    std::shared_ptr<std::vector<Generation_result>> ss = std::make_shared<std::vector<Generation_result>>();
    std::vector<root> p;

    //get those roots that are in range of (p_min, p_max)
    uint index = 0;
    while (roots.size() > index && roots[index].simple < p_min)
        index++;

    if (index == roots.size() - 1)
    {
        qDebug() << "Error: p_min is too big";
        emit generationError(std::make_shared<QString>("p_min слишком велико"));
        return;
    }

    while (roots.size() > index && roots[index].simple <= p_max)
    {
        if ((roots[index].simple - 1) % f == 0)
            p.push_back(roots[index]);
        index++;
    }

    //iterate over the roots and make a generation for each one of them
    for (auto& val : p)
    {
        std::vector<int> s;

        uint32_t sn = static_cast<uint32_t>(pow(val.simple, n));
        if ((val.g % 2) == 0)
            val.g += sn;

        uint32_t e = (val.simple - 1) / f;

        uint32_t mod = 2 * sn;

        s.resize(mod);
        std::fill(s.begin(), s.end(), a);

        s[sn] = c;

        std::vector<int> dj;
        dj.resize(n + 1);

        for (uint32_t j = 1; j <= n; j++)
        {
            dj[j] = pow(val.simple, j - 1) * f;

            for (uint32_t i = 0; i < e; i++)
            {
                //check whether generation should be stopped
                if (should_stop.load())
                {
                    emit generationStopped();
                    return;
                }

                int64_t temp = modpow(val.simple, n - j, mod);
                for (int t = dj[j] / 2; t < dj[j]; t++)
                {
                    int64_t m = temp * modpow(val.g, t + i * dj[j], mod);
                    //s[m] = b
                    s[fMod(m, mod)] = b;
                    //s[2m] = d
                    s[fMod(2 * m, mod)] = d;
                }
                for (int t = 0; t < dj[j] / 2; t++)
                {
                    int64_t m2 = 2 * temp * modpow(val.g, t + i * dj[j], mod);
                    s[fMod(m2, mod)] = c;
                }
            }
        }

        Generation_result gen_res { s, val.simple, mod, n, f, a, b, c, d };
        ss->push_back(gen_res);
    }
    emit generationCompleted(ss);
}
