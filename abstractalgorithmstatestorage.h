#ifndef ABSTRACTALGORITHMSTATESTORAGE_H
#define ABSTRACTALGORITHMSTATESTORAGE_H
#include <QObject>
#include <QString>
#include <memory>
#include <vector>

class AbstractAlgorithmStateStorage : public QObject
{
    Q_OBJECT
public:
    AbstractAlgorithmStateStorage();
    virtual ~AbstractAlgorithmStateStorage();

    virtual bool putName(const QString& data) = 0;
    virtual bool putInt(const QString& name, const int data) = 0;
    virtual bool putUInt(const QString& name, const uint data) = 0;
    virtual bool putVector(const QString& name, const std::vector<int>& data) = 0;
    virtual bool putVectorOfVectors(const QString& name, const std::vector<std::vector<int>>& data) = 0;

    virtual bool saveIt(const QString& path) = 0;

    virtual bool loadIt(const QString& path) = 0;

    virtual bool getName(QString& result) = 0;
    virtual bool getInt(const QString& name, int& result) = 0;
    virtual bool getUInt(const QString& name, uint& data) = 0;
    virtual bool getVector(const QString& name, std::vector<int>& result) = 0;
    virtual bool getVectorOfVectors(const QString& name, std::vector<std::vector<int>> & result) = 0;

    virtual void clear() = 0;

signals:
    void sendMsg(std::shared_ptr<QString> msg);
};

#endif // ABSTRACTALGORITHMSTATESTORAGE_H
