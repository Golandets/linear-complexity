#ifndef BERLEKEMP_MESSY_ALG_H
#define BERLEKEMP_MESSY_ALG_H

#include <QDebug>
#include <math.h>
#include <vector>

#include "abstract_algorithm.h"

class Berlekemp_Messy_alg : public Abstract_algorithm
{
public:
    Berlekemp_Messy_alg(const std::shared_ptr<AbstractAlgorithmStateStorage> i_state_saver);

    int calculate() override;

    int resumeCalculations() override;

    void clear();

    // Abstract_algorithm interface
public:
    virtual void restore(const QString& path) override;
    virtual void store(const QString& path) const override;

protected:
    virtual QString restoreName(const QString& path) const override;

private:
    int doCalculations();
    std::vector<int> add(const std::vector<int> val1, const std::vector<int> val2) const;

    std::vector<int> sub(const std::vector<int> val1, const std::vector<int> val2) const;

    std::vector<int> mult(const std::vector<int> val1, const std::vector<int> val2) const;

    std::vector<int> mult(const std::vector<int> val1, const int val2) const;

    uint get_num_of_vals(const std::vector<int>& val) const;

    QString val_to_str(const int val, const int pos) const;

    void print_vector(const std::vector<int>& val, const QString&& name) const;

    int add(const int a, const int b) const;

    int mult(const int a, const int b) const;

    std::vector<int> g;
    std::vector<int> h;
    int b;
    int m;
    std::vector<int> gG;

    std::vector<int> copyg;
    std::vector<int> copyh;
    std::vector<int> copyS;
    int copyb;
    int copym;

    const QString storageBName = "b";
    const QString storageMName = "m";
    const QString storageGName = "g";
    const QString storageHName = "h";

    std::vector<std::vector<int>> plus_table = {
        { 0, 1, 2, 3 },
        { 1, 0, 3, 2 },
        { 2, 3, 0, 1 },
        { 3, 2, 1, 0 }
    };
    std::vector<std::vector<int>> mult_table = {
        { 0, 0, 0, 0 },
        { 0, 1, 2, 3 },
        { 0, 2, 3, 1 },
        { 0, 3, 1, 2 }
    };
};

#endif // BERLEKEMP_MESSY_ALG_H
