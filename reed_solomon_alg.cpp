#include "reed_solomon_alg.h"
//#define DEBUG_ON

Reed_Solomon_alg::Reed_Solomon_alg(const std::shared_ptr<AbstractAlgorithmStateStorage> i_state_saver)
    : Abstract_algorithm("Алгоритм Рида-Соломона", i_state_saver)
{
}

int Reed_Solomon_alg::calculate()
{

    a.resize(e);
    b.resize(e);
    a_new.resize(e);
    b_new.resize(e);
    t.resize(e);
    u.resize(e);
#ifdef DEBUG_ON
    QString debug_info = "k=0: ";
    QString temp_str = "k=1: ";
    QString debug_info2 = "k=0: ";
#endif //DEBUG_ON
    for (uint i = 0; i < e; i++)
    {
        a[i].resize(1);
        b[i].resize(1);
        a_new[i].resize(1);
        b_new[i].resize(1);

        int p_i = static_cast<int>(pow(p, i));
        a[i][0] = p_i;
        b[i][0] = 0;
        a_new[i][0] = p_i;
        b_new[i][0] = p_i * S[0];
        calc_t_u(S[0], p, e, i, t[i], u[i]);

#ifdef DEBUG_ON
        debug_info.append("(").append(getFormStr(a[i])).append(", ").append(getFormStr(b[i])).append(")");
        temp_str.append("(").append(getFormStr(a[i])).append(", ").append(getFormStr(b[i])).append(")");
        debug_info2.append(QString::number(L(a[i], b[i]))).append(",").append(QString::number(u[i])).append(",").append(QString::number(t[i]));
#endif //DEBUG_ON
    }

#ifdef DEBUG_ON
    qDebug() << debug_info;
    qDebug() << temp_str;
    qDebug() << debug_info2;
#endif //DEBUG_ON

    a_old.resize(e);
    b_old.resize(e);
    t_old.resize(e);
    u_old.resize(e);
    r.resize(e);

    step = 1;
    return doCalculations();
}

int Reed_Solomon_alg::resumeCalculations()
{
    return doCalculations();
}

void Reed_Solomon_alg::restore(const QString& path)
{
    //stores all inside data in storage
    if (stateSaver != nullptr && stateSaver->loadIt(path))
    {
        stateSaver->getUInt(storageStepName, step);
        stateSaver->getUInt(storagePName, p);
        stateSaver->getUInt(storageEName, e);

        stateSaver->getVector(storageSName, S);

        stateSaver->getVectorOfVectors(storageAName, a);
        stateSaver->getVectorOfVectors(storageBName, b);
        stateSaver->getVectorOfVectors(storageANewName, a_new);
        stateSaver->getVectorOfVectors(storageBNewName, b_new);
        stateSaver->getVectorOfVectors(storageAOldName, a_old);
        stateSaver->getVectorOfVectors(storageBOldName, b_old);

        stateSaver->getVector(storageTName, t);
        stateSaver->getVector(storageUName, u);
        stateSaver->getVector(storageTOldName, t_old);
        stateSaver->getVector(storageUOldName, u_old);
        stateSaver->getVector(storageRName, r);

        restoreGeneration();

        emit restoreSuccess();
    }
    else
        emit restoreError(std::make_shared<QString>("Не удалось открыть файл " + path));
}

void Reed_Solomon_alg::store(const QString& path) const
{
    //restores all data from storage
    if (stateSaver != nullptr)
    {
        stateSaver->clear();

        stateSaver->putName(getName());

        stateSaver->putUInt(storageStepName, step);
        stateSaver->putUInt(storagePName, p);
        stateSaver->putUInt(storageEName, e);

        stateSaver->putVector(storageSName, S);

        stateSaver->putVectorOfVectors(storageAName, a);
        stateSaver->putVectorOfVectors(storageBName, b);
        stateSaver->putVectorOfVectors(storageANewName, a_new);
        stateSaver->putVectorOfVectors(storageBNewName, b_new);
        stateSaver->putVectorOfVectors(storageAOldName, a_old);
        stateSaver->putVectorOfVectors(storageBOldName, b_old);

        stateSaver->putVector(storageTName, t);
        stateSaver->putVector(storageUName, u);
        stateSaver->putVector(storageTOldName, t_old);
        stateSaver->putVector(storageUOldName, u_old);
        stateSaver->putVector(storageRName, r);

        storeGeneration();

        if(stateSaver->saveIt(path))
            emit storeSuccess();
        else
            emit storeError(std::make_shared<QString>("Не удалось сохранить файл " + path));
    }
}

QString Reed_Solomon_alg::restoreName(const QString& path) const
{
    QString name;
    if (stateSaver != nullptr && stateSaver->loadIt(path))
        stateSaver->getName(name);
    return name;
}

int Reed_Solomon_alg::doCalculations()
{
    //main cycle of the algorithm
    should_stop.store(false);

    int pe = static_cast<int>(pow(p, e));

    mult_table = calcMultTable(pe);

    for (; step < S.size(); step++)
    {
        if (should_stop.load())
        {
            qDebug() << "user stopped";
            return -1;
        }
#if SHOW_PROGRESS
        float progress = step * 100.0 / S.size();
        emit update_progress(progress);
#endif //SHOW_PROGRESS

#ifdef DEBUG_ON
        qDebug() << "step " << k;

        debug_info = "k=" + QString::number(k + 1) + ": ";
        debug_info2 = "k=" + QString::number(k) + ": ";
#endif //DEBUG_ON

        //first step
        for (uint g = 0; g < e; g++)
        {
            if (L(a_new[g], b_new[g]) > L(a[g], b[g]))
            {
                uint h = e - 1 - u[g];
                a_old[g] = a[h];
                b_old[g] = b[h];
                t_old[g] = t[h];
                u_old[g] = u[h];
                r[g] = step - 1;
            }
        }
        //second step
        a = a_new;
        b = b_new;

        //third step
        for (uint i = 0; i < e; i++)
        {
            std::vector<int> temp = massMultMass(modK(a.at(i), step), modK(S, step), pe);
            temp = massPlusMass(temp, massMultVar(modK(b.at(i), step), -1, pe), pe);

            calc_t_u(temp.at(step), p, e, t[i], u[i]);

            uint g = e - 1 - u[i];

            a_new[i] = a[i];
            b_new[i] = b[i];

            QString temp_text = "*";
            if (u[i] == e)
            {
                temp_text = "I";
            }
            else if (L(a[g], b[g]) == 0)
            {
                std::vector<int> tempL = { 1 };
                tempL = massMultX(tempL, step);
                tempL = massMultVar(tempL, t[i] * pow(p, u[i]), pe);
                b_new[i] = massPlusMass(b_new[i], tempL, pe);
                temp_text = "IIa";
            }
            else
            {
                int temp = divide(t[i], t_old[g], pe);
                temp = fMod(-temp, pe);
                if (u[i] - u_old[g] >= 0)
                    temp = fMod(temp * pow(p, u[i] - u_old[g]), pe);
                else
                    temp = divide(temp, pow(p, u_old[g] - u[i]), pe);

                std::vector<int> tempL = massMultX(a_old[g], step - r[g]);
                tempL = massMultVar(tempL, temp, pe);
                a_new[i] = massPlusMass(a_new[i], tempL, pe);

                tempL = massMultX(b_old[g], step - r[g]);
                tempL = massMultVar(tempL, temp, pe);
                b_new[i] = massPlusMass(b_new[i], tempL, pe);

                temp_text = "IIb";
            }
#ifdef DEBUG_ON
            debug_info.append("(").append(getFormStr(a_new[i])).append(", ").append(getFormStr(b_new[i])).append(")");
            debug_info2.append(QString::number(L(a[i], b[i]))).append(",").append(QString::number(u[i])).append(",").append(QString::number(t[i])).append(",").append(temp_text).append(" ");
#endif //DEBUG_ON
        }
#ifdef DEBUG_ON
        qDebug() << debug_info;
        qDebug() << debug_info2;
#endif //DEBUG_ON
    }
    int l = L(a_new[0], b_new[0]);

#ifdef DEBUG_ON
    qDebug() << "L = " << l;
#endif //DEBUG_ON
    return l;
}

void Reed_Solomon_alg::calc_t_u(const int s, const int p, const int e, const int n, int& t, int& u) const
{
    int epsilon;
    int sigma;
    if (s == 0)
    {
        epsilon = e;
        sigma = 1;
    }
    else
    {
        epsilon = s / p;
        sigma = s % p;
    }

    if (n + epsilon < e)
    {
        t = sigma;
        u = n + epsilon;
    }
    else
    {
        t = 1;
        u = e;
    }
}

void Reed_Solomon_alg::calc_t_u(const int s, const int p, const int e, int& t, int& u) const
{
    if (s == 0)
    {
        t = 1;
        u = e;
    }
    else
    {
        u = 0;
        int ts = s;
        while (ts % p == 0)
        {
            ts /= p;
            u++;
        }
        if (u != 0)
            t = s / (u * p);
        else
            t = s;
    }
}

int Reed_Solomon_alg::L(const std::vector<int> a, const std::vector<int> b) const
{
    int max_a = -1;
    int max_b = -1;

    for (int i = a.size() - 1; i >= 0; i--)
    {
        if (a[i] != 0)
        {
            max_a = i;
            break;
        }
    }

    for (int i = b.size() - 1; i >= 0; i--)
    {
        if (b[i] != 0)
        {
            max_b = i;
            break;
        }
    }

    return std::max(max_a, max_b + 1);
}

std::vector<int> Reed_Solomon_alg::modK(const std::vector<int> input, const int k) const
{
    if (input.size() <= static_cast<uint>(k + 1))
        return input;

    std::vector<int> res;
    res.resize(k + 1);
    for (uint i = 0; i < res.size(); i++)
        res[i] = input[i];
    return res;
}

std::vector<int> Reed_Solomon_alg::massMultMass(const std::vector<int> a, const std::vector<int> b, const int p) const
{
    std::vector<int> res;
    res.resize(a.size() + b.size());
    for (uint i = 0; i < b.size(); i++)
        for (uint j = 0; j < a.size(); j++)
            res[i + j] = fMod(res[i + j] + a[j] * b[i], p);
    return res;
}

std::vector<int> Reed_Solomon_alg::massMultVar(const std::vector<int> a, const int b, const int p) const
{
    std::vector<int> res;
    res.resize(a.size());
    for (uint i = 0; i < res.size(); i++)
        res[i] = fMod(a[i] * b, p);
    return res;
}

std::vector<int> Reed_Solomon_alg::massPlusMass(const std::vector<int> a, const std::vector<int> b, const int p) const
{
    std::vector<int> res;
    res.resize(std::max(a.size(), b.size()));
    for (uint i = 0; i < res.size(); i++)
    {
        int sum = 0;
        if (i < a.size())
            sum += a[i];
        if (i < b.size())
            sum += b[i];
        res[i] = fMod(sum, p);
    }
    return res;
}

std::vector<int> Reed_Solomon_alg::massMultX(const std::vector<int> a, const int b) const
{
    std::vector<int> res;
    res.resize(a.size() + b);
    for (uint i = b; i < b + a.size(); i++)
        res[i] = a[i - b];
    return res;
}

std::vector<std::vector<int>> Reed_Solomon_alg::calcMultTable(const int p) const
{
    std::vector<std::vector<int>> res;
    res.resize(p);
    for (int i = 0; i < p; i++)
    {
        res[i].resize(p);
        for (int j = 0; j < p; j++)
            res[i][j] = fMod(i * j, p);
    }
    return res;
}

int Reed_Solomon_alg::divide(const int divisible, const int divider, const int p) const
{
    int divis = fMod(divisible, p);
    int divid = fMod(divider, p);
    for (uint i = 0; i < mult_table[divid].size(); i++)
        if (mult_table[divid][i] == divis)
            return i;
    return 0;
}
#ifdef DEBUG_ON
QString Reed_Solomon_alg::getFormStr(const std::vector<int> vF) const
{
    QString vRez = "";
    int notUse = 0;
    for (int i = vF.size() - 1; i >= 0; i--)
        if (vF[i] != 0)
        {
            if ((notUse != 0) && (i != static_cast<int>(vF.size() - 1)))
                vRez += " + "; // знак

            if (vF[i] == -1) // число при x
                vRez += " -";
            else if (vF[i] != 1)
                vRez += QString::number(vF[i]);

            if (i > 1)
                vRez += "x^" + QString::number(i);
            else if (i == 1)
                vRez += "x";

            if ((vF[i] == 1) && (i == 0))
                vRez += "1";
            notUse++;
        }
    if (vRez == "")
        vRez = "0";
    return vRez;
}
#endif
