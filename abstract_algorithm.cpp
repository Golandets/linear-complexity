#include "abstract_algorithm.h"
#include <qglobal.h>

Abstract_algorithm::Abstract_algorithm(const QString& i_name, const std::shared_ptr<AbstractAlgorithmStateStorage> i_state_saver)
    : name(i_name)
    , p(2)
    , e(2)
    , step(0)
    , stateSaver(i_state_saver)
{
    should_stop.store(false);
}

void Abstract_algorithm::init(Generation::Generation_result &gen, const uint i_p, const uint i_e)
{
    //all valus are duplicated so algorithms can work correctly
    auto size = gen.generation.size();
    S.resize(size * 2);
    for (uint i = 0; i < size; i++)
    {
        S[i] = gen.generation[i];
        S[i + size] = gen.generation[i];
    }

    generation = gen;
    p = i_p;
    e = i_e;
}

void Abstract_algorithm::stop()
{
    should_stop.store(true);
}

QString Abstract_algorithm::getName() const
{
    return name;
}

Generation::Generation_result Abstract_algorithm::getGeneration() const
{
    return generation;
}

bool Abstract_algorithm::canRestore(const QString& path) const
{
    return restoreName(path) == getName();
}

void Abstract_algorithm::restoreGeneration()
{
    stateSaver->getVector(storageGenName, generation.generation);

    stateSaver->getUInt(storageGenPName, generation.p);
    stateSaver->getUInt(storageGenPn2Name, generation.pn_2);
    stateSaver->getUInt(storageGenNName, generation.n);
    stateSaver->getUInt(storageGenFName, generation.f);

    stateSaver->getInt(storageGenAName, generation.a);
    stateSaver->getInt(storageGenBName, generation.b);
    stateSaver->getInt(storageGenCName, generation.c);
    stateSaver->getInt(storageGenDName, generation.d);
}

void Abstract_algorithm::storeGeneration() const
{
    stateSaver->putVector(storageGenName, generation.generation);

    stateSaver->putUInt(storageGenPName, generation.p);
    stateSaver->putUInt(storageGenPn2Name, generation.pn_2);
    stateSaver->putUInt(storageGenNName, generation.n);
    stateSaver->putUInt(storageGenFName, generation.f);

    stateSaver->putInt(storageGenAName, generation.a);
    stateSaver->putInt(storageGenBName, generation.b);
    stateSaver->putInt(storageGenCName, generation.c);
    stateSaver->putInt(storageGenDName, generation.d);
}

int Abstract_algorithm::fMod(const int a, const int p) const
{
    if (p == 0)
        return a;
    if (a >= 0)
        return (a % p);
    else
        return (p + a % p);
}
