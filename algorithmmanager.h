#ifndef ALGORITHMMANAGER_H
#define ALGORITHMMANAGER_H

#include <QObject>
#include "abstract_algorithm.h"
#include <future>

class AlgorithmManager : public QObject
{
    Q_OBJECT
public:
    explicit AlgorithmManager(const uint i_p, const uint i_e);

    ~AlgorithmManager();

    void startCalculations(std::shared_ptr<Abstract_algorithm> i_alg,
                           std::shared_ptr<std::vector<Generation::Generation_result>> generations,
                           const QString & i_fileName, const bool i_saveGen);
    void resumeCalculations(const QString & i_fileName, const bool i_saveGen);

    void stopCalculations();

protected:
    void saveToFile(Generation::Generation_result gen, int result);
    void initFile();

    std::shared_ptr<Abstract_algorithm> algorithm;
    std::vector<int> results;
    const uint p;
    const uint e;
    const QString initFileStr;
    QString filename;
    bool saveGenerations;
    std::atomic<bool> stopped;
    QFile output_file;
    std::future<void> fut;

signals:
    void currentCalcFinished(const int);
    void allCalcFinished(std::shared_ptr<std::vector<int>>);
    void calcStopped();
    void calcError(const QString&);
};

#endif // ALGORITHMMANAGER_H
