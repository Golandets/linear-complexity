#ifndef REED_SOLOMON_ALG_H
#define REED_SOLOMON_ALG_H
#include <QDebug>
#include <math.h>
#include <vector>

#include "abstract_algorithm.h"

class Reed_Solomon_alg : public Abstract_algorithm
{
public:
    Reed_Solomon_alg(const std::shared_ptr<AbstractAlgorithmStateStorage> i_state_saver);

    // Abstract_algorithm interface
public:
    int calculate() override;
    virtual int resumeCalculations() override;

    virtual void restore(const QString& path) override;
    virtual void store(const QString& path) const override;

protected:
    virtual QString restoreName(const QString& path) const override;

private:
    int doCalculations();
    void calc_t_u(const int s, const int p, const int e, const int n, int& t, int& u) const;

    void calc_t_u(const int s, const int p, const int e, int& t, int& u) const;
    int L(const std::vector<int> a, const std::vector<int> b) const;

    std::vector<int> modK(const std::vector<int> input, const int k) const;

    std::vector<int> massMultMass(const std::vector<int> a, const std::vector<int> b, const int p) const;

    std::vector<int> massMultVar(const std::vector<int> a, const int b, const int p) const;

    std::vector<int> massPlusMass(const std::vector<int> a, const std::vector<int> b, const int p) const;

    std::vector<int> massMultX(const std::vector<int> a, const int b) const;

    std::vector<std::vector<int>> calcMultTable(const int p) const;

    int divide(const int divisible, const int divider, const int p) const;

#ifdef DEBUG_ON
    QString getFormStr(const std::vector<int> vF) const;
#endif

    std::vector<std::vector<int>> mult_table;

    std::vector<std::vector<int>> a;
    std::vector<std::vector<int>> b;
    std::vector<std::vector<int>> a_new;
    std::vector<std::vector<int>> b_new;
    std::vector<int> t;
    std::vector<int> u;
    std::vector<std::vector<int>> a_old;
    std::vector<std::vector<int>> b_old;
    std::vector<int> t_old;
    std::vector<int> u_old;
    std::vector<int> r;

    const QString storageAName = "a";
    const QString storageBName = "b";
    const QString storageANewName = "a_new";
    const QString storageBNewName = "b_new";
    const QString storageTName = "t";
    const QString storageUName = "u";
    const QString storageAOldName = "a_old";
    const QString storageBOldName = "b_old";
    const QString storageTOldName = "t_old";
    const QString storageUOldName = "u_old";
    const QString storageRName = "r";
};

#endif // REED_SOLOMON_ALG_H
